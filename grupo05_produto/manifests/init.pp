# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include grupo05_produto
class grupo05_produto {
group { 'java':
       ensure => 'present',
       gid    => '501',
     }

  user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }

  file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }

  file { '/opt/apps/produto':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
  package { 'java-1.8.0-openjdk-devel':
        ensure => installed,
        name   => $java,
    }


  exec { 'produto-get':
    command => "/usr/bin/wget -q http://23.96.48.125/artfactory/grupo5/produto/produto.jar -O /opt/apps/produto/produto.jar",
    creates => "/opt/apps/produto/produto.jar" 
  }

exec { 'produto-start':
    cwd => '/opt/apps/produto/',
    command => '/usr/bin/java -jar -javaagent:/opt/appdynamics/javaagent.jar produto.jar',
    path => '/usr/bin/:/bin/',
}
  
service { 'firewalld':
  ensure => 'stopped',
  enable => 'false'
}

  file { '/opt/apps/produto/produto.jar':
    mode => '0755',
    require => Exec[ 'produto-get', 'produto-start']
  }

}
